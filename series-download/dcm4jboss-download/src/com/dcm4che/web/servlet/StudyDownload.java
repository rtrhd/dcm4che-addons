package com.dcm4che.web.servlet;

import java.io.BufferedInputStream;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Random;
import java.util.Set;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.dcm4che.data.Dataset;
import org.dcm4che.data.DcmObjectFactory;
import org.dcm4che.dict.Tags;
import org.dcm4che2.audit.message.AuditMessage;
import org.dcm4che2.audit.message.InstancesTransferredMessage;
import org.dcm4che2.audit.message.ParticipantObjectDescription;
import org.dcm4che2.audit.util.InstanceSorter;
import org.dcm4chex.archive.common.Availability;
import org.dcm4chex.archive.dcm.AbstractScpService;
import org.dcm4chex.archive.ejb.interfaces.FileSystemMgt2;
import org.dcm4chex.archive.ejb.interfaces.FileSystemMgt2Home;
import org.dcm4chex.archive.ejb.jdbc.FileInfo;
import org.dcm4chex.archive.ejb.jdbc.RetrieveCmd;
import org.dcm4chex.archive.util.EJBHomeFactory;
import org.dcm4chex.archive.util.FileUtils;
import org.dcm4chex.archive.util.HomeFactoryException;
import org.jboss.system.server.ServerConfigLocator;

public class StudyDownload extends HttpServlet {

    private static final long serialVersionUID = -8113434693690546693L;

    private static final int BUFF_BYTES = 1024 * 1024;

    private static Logger logger = Logger.getLogger(StudyDownload.class.getName());

    private static final DcmObjectFactory dof = DcmObjectFactory.getInstance();

    private static final String[] DCM4CHEE = { "DCM4CHEE" };


    /**
     * Default constructor.
     */
    public StudyDownload() {
        super();
    }


    /**
     * @see javax.servlet.http.HttpServlet#doGet(javax.servlet.http.HttpServletRequest,
     *      javax.servlet.http.HttpServletResponse)
     */
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doPost(req, resp);
    }


    /**
     * @see javax.servlet.http.HttpServlet#doPost(javax.servlet.http.HttpServletRequest,
     *      javax.servlet.http.HttpServletResponse)
     */
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String studyUID     = req.getParameter("studyUID");
        String patientID    = req.getParameter("patientID");
        String seriesUID    = req.getParameter("seriesUID");
        String studyID      = req.getParameter("studyID");
        String seriesNumber = req.getParameter("seriesNumber");
        String modality     = req.getParameter("modality");

        if (studyUID == null || studyUID.length() == 0) {
            resp.setContentType("text/plain");
            resp.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            PrintWriter out = resp.getWriter();
            out.write("studyUID parameter is required!");
            out.flush();
            return;
        }

        // if pat id / study id / series no. missing then fake them out
        if (patientID == null || patientID.length() == 0) {
            patientID = "NOPAT";
        }    
        if (studyID == null || studyID.length() == 0) {
            studyID = studyUID.substring(Math.max(0, studyUID.length()-5));
        }
        if (seriesNumber == null || seriesNumber.length() == 0) {
            if (seriesUID != null && seriesNumber.length() > 0) {
                seriesNumber = seriesUID.substring(Math.max(0, seriesUID.length() - 5));
            } else {
                seriesNumber = "0000";
            }
        }
        if (modality == null || modality.length() == 0) {
            modality = "XX";
        }

        // construct a suggested filename for the downloaded
        String downloadFilename = 
                patientID.substring(0, Math.min(15, patientID.length()))
              + "-"
              + studyID.substring(0, Math.min(10, studyID.length()));

        if (seriesUID == null || seriesUID.length() == 0) {
            // assume a study level request
            downloadFilename = "Study-" + downloadFilename;
        } else {
            // assume a series level request
            downloadFilename =
                modality + "-" + downloadFilename + "-"
                         + seriesNumber.substring(Math.max(0, seriesNumber.length() - 5));
        }
 
        if (logger.isInfoEnabled()) {
            String log = "Study download request for studyUid [ " + studyUID + " ]";
            if (seriesUID != null) {
                log += " and seriesUid " + seriesUID;
            }
            logger.info(log);
        }

        File zipOutput = null;
        ServletOutputStream op = resp.getOutputStream();
        DataInputStream in = null;
        try {
            zipOutput = createStudyZip(studyUID, seriesUID, modality, req);
            if (zipOutput != null) {
                resp.setContentType("application/zip");
                //resp.setContentType("application/dicom-archive");
                resp.setContentLength((int) zipOutput.length());
                resp.setHeader("Content-Disposition", "inline;filename=" + downloadFilename + ".zip");
                //resp.setHeader("Content-Disposition", "inline;filename=" + downloadFilename + ".dcmzip");
                byte[] bbuf = new byte[2048];
                in = new DataInputStream(new FileInputStream(zipOutput));
                int length = 0;
                while ((length = in.read(bbuf)) != -1) {
                    op.write(bbuf, 0, length);
                }
                op.flush();
            }
        } catch (Throwable th) {
            logger.error("An error occurred while attempting to prepare zip file for download." + th);
            resp.setContentType("text/plain");
            resp.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            PrintWriter out = resp.getWriter();
            out.write("ERROR: " + th.getMessage());
            out.flush();
        } finally {
            if (in != null)
                in.close();
            if (op != null)
                op.close();
            if (zipOutput != null && zipOutput.exists()) {
                if (!zipOutput.delete())
                    logger.warn("Unable to delete temp zip file: " + zipOutput.getPath());
            }
        }
    }


    public File createStudyZip(String suid, String seruid, String modality, HttpServletRequest req) throws Exception {
        Dataset ds = dof.newDataset();
        ds.putLO(Tags.PatientID, "*");
        if (seruid != null ) {
            ds.putCS(Tags.QueryRetrieveLevel, "SERIES");
            ds.putUI(Tags.SeriesInstanceUID, seruid);
            ds.putUI(Tags.StudyInstanceUID, suid);
        } else {
            ds.putCS(Tags.QueryRetrieveLevel, "STUDY");
            ds.putUI(Tags.StudyInstanceUID, suid);
        }
        FileInfo[][] fileInfos = RetrieveCmd.create(ds).getFileInfos();
        return retrieveFiles(suid, modality, fileInfos, req);
    }


    private File retrieveFiles(String suid, String modality, FileInfo[][] fileInfos, HttpServletRequest req) {
        List<FileInfo> successfulTransferred = new ArrayList<FileInfo>();
        RetrieveInfo retrieveInfo = new RetrieveInfo(fileInfos);
        Set<String> studyInfos = new HashSet<String>();
        Collection localFiles = retrieveInfo.getLocalFiles();
        File zipOutput = createZipFile(suid);
        FileOutputStream fos = null;
        ZipOutputStream zos = null;
        try {
            fos = new FileOutputStream(zipOutput);
            zos = new ZipOutputStream(fos);

            for (Iterator iter = localFiles.iterator(); iter.hasNext();) {
                final List list = (List) iter.next();
                final FileInfo fileInfo = (FileInfo) list.get(0);
                File f = null;
                try {
                    f = FileUtils.toFile(fileInfo.basedir, fileInfo.fileID);
                    ZipEntry zipEntry = new ZipEntry(modality + f.getName() + ".dcm");
                    zos.putNextEntry(zipEntry);
                    if (logger.isDebugEnabled()) {
                        logger.debug("Copying " + f.getAbsolutePath() + " to zip");
                    }
                    copyTo(f, zos);
                    successfulTransferred.add(fileInfo);
                } catch (Exception e) {
                    logger.error("Could not zip file: " + f + " iuid: " + fileInfo.sopIUID, e);
                }

                if (fileInfo.availability == Availability.ONLINE) {
                    // only track access on ONLINE FS
                    studyInfos.add(fileInfo.studyIUID + '@' + fileInfo.basedir);
                }
            }
        } catch (Exception e) {
            logger.error("Could not package study for download: " + suid);
        } finally {
            if (zos != null) {
                try {
                    zos.flush();
                    zos.close();
                } catch (IOException ignore) {}
            }
            if (fos != null) {
                try {
                    fos.close();
                } catch (IOException ignore) {}
            }
        }

        if (!successfulTransferred.isEmpty()) {
            logger.info("Successfully packaged study for download: " + suid);
            logInstancesSent(req, successfulTransferred);
        }

        updateStudyAccessTime(studyInfos);
        return zipOutput;
    }


    private File createZipFile(String studyUid) {
        File dir = new File(ServerConfigLocator.locate().getServerHomeDir(), "ziptemp");
        if (!dir.exists()){
            dir.mkdir();
        }
        File file = null;
        do {
            file = new File(dir, studyUid + "_" + new Random().nextInt() % 10000 + ".zip");
        } while (file.exists());

        return file;
    }


    private void copyTo(File file, OutputStream os) throws FileNotFoundException, IOException {
        byte[] buff = new byte[BUFF_BYTES];

        int bytes;
        BufferedInputStream is = null;
        FileInputStream fis = null;

        try {
            fis = new FileInputStream(file);
            is = new BufferedInputStream(fis, BUFF_BYTES);

            while ((bytes = is.read(buff, 0, BUFF_BYTES)) >= 0) {
                os.write(buff, 0, bytes);
            }
        } finally {
            try {
                if (is != null)
                    is.close();
                is = null;
            } catch (Exception ignore) {}
            try {
                if (fis != null)
                    fis.close();
                fis = null;
            } catch (Exception ignore) {}
        }
    }


    protected void logInstancesSent(HttpServletRequest req, List fileInfos) {
        try {
            InstanceSorter sorter = new InstanceSorter();
            FileInfo fileInfo = null;
            for (Iterator iter = fileInfos.iterator(); iter.hasNext();) {
                fileInfo = (FileInfo)iter.next();
                sorter.addInstance(fileInfo.studyIUID, fileInfo.sopCUID, fileInfo.sopIUID, null);
            }
            String destAET = req.getRemoteUser();
            String destHost = req.getRemoteAddr();
            if (destAET == null)
                destAET = destHost;
            InstancesTransferredMessage msg = new InstancesTransferredMessage(InstancesTransferredMessage.EXECUTE);

            msg.addSourceProcess(AuditMessage.getProcessID(), DCM4CHEE, AuditMessage.getProcessName(), AuditMessage
                    .getLocalHostName(), false);
            msg.addDestinationProcess(destHost, new String[] { destAET }, null, destHost, true);
            if (fileInfo != null) {
                msg.addPatient(fileInfo.patID, AbstractScpService.formatPN(fileInfo.patName));
            } else {
                msg.addPatient("UNKNOWN", AbstractScpService.formatPN("UNKNOWN"));
            }
            for (Iterator iter = sorter.getSUIDs().iterator(); iter.hasNext();) {
                String suid = (String) iter.next();
                ParticipantObjectDescription desc = new ParticipantObjectDescription();
                for (Iterator iter2 = sorter.getCUIDs(suid).iterator(); iter2.hasNext();) {
                    String cuid = (String) iter2.next();
                    ParticipantObjectDescription.SOPClass sopClass = new ParticipantObjectDescription.SOPClass(cuid);
                    sopClass.setNumberOfInstances(sorter.countInstances(suid, cuid));
                    desc.addSOPClass(sopClass);
                }
                msg.addStudy(suid, desc);
            }
            msg.validate();
            Logger.getLogger("auditlog").info(msg);
        } catch (Exception e) {
            logger.warn("Audit Log failed:", e);
        }
    }


    void updateStudyAccessTime(Set studyInfos) {
        FileSystemMgt2 fsMgt;
        try {
            fsMgt = getFileSystemMgtHome().create();
        } catch (Exception e) {
            logger.error("Failed to access FileSystemMgt EJB");
            return;
        }
        try {
            for (Iterator it = studyInfos.iterator(); it.hasNext();) {
                String studyInfo = (String) it.next();
                int delim = studyInfo.indexOf('@');
                try {
                    fsMgt.touchStudyOnFileSystem(studyInfo.substring(0, delim), studyInfo.substring(delim + 1));
                } catch (Exception e) {
                    logger.warn("Failed to update access time for study " + studyInfo, e);
                }
            }
        } finally {
            try {
                fsMgt.remove();
            } catch (Exception ignore) {}
        }
    }

    FileSystemMgt2Home getFileSystemMgtHome() throws HomeFactoryException {
        return (FileSystemMgt2Home)EJBHomeFactory.getFactory().lookup(FileSystemMgt2Home.class,
                FileSystemMgt2Home.JNDI_NAME);
    }
}
