package com.dcm4che.web.servlet;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Set;

import org.dcm4chex.archive.common.Availability;
import org.dcm4chex.archive.ejb.jdbc.FileInfo;

final class RetrieveInfo {

    static class IuidsAndTsuids {
        final HashSet<String> iuids = new HashSet<String>();

        final HashSet<String> tsuids = new HashSet<String>();
    }

    private final int size;

    private final HashMap<String, IuidsAndTsuids> iuidsAndTsuidsByCuid = new HashMap<String, IuidsAndTsuids>();

    private final LinkedHashMap<String, List> localFilesByIuid = new LinkedHashMap<String, List>();

    private final HashSet<String> notAvailableIuids = new HashSet<String>();

    RetrieveInfo(FileInfo[][] instInfos) {
        FileInfo[] fileInfos;
        FileInfo fileInfo;
        String iuid, cuid;
        IuidsAndTsuids iuidsAndTsuids;
        this.size = instInfos.length;
        for (int i = 0; i < size; ++i) {
            fileInfos = instInfos[i];
            iuid = fileInfos[0].sopIUID;
            cuid = fileInfos[0].sopCUID;

            // Check the availability first. If it's not ONLINE or NEARLINE,
            // we skip it.
            if (fileInfos[0].availability >= Availability.OFFLINE)
                continue;

            iuidsAndTsuids = iuidsAndTsuidsByCuid.get(cuid);
            if (iuidsAndTsuids == null) {
                iuidsAndTsuids = new IuidsAndTsuids();
                iuidsAndTsuidsByCuid.put(cuid, iuidsAndTsuids);
            }
            iuidsAndTsuids.iuids.add(iuid);
            notAvailableIuids.add(iuid);
            for (int j = 0; j < fileInfos.length; j++) {
                fileInfo = fileInfos[j];
                putLocalFile(iuid, fileInfo);
                iuidsAndTsuids.tsuids.add(fileInfo.tsUID);
            }
        }
    }

    @SuppressWarnings("unchecked")
    private void putLocalFile(String iuid, FileInfo fileInfo) {
        List<FileInfo> localFiles = localFilesByIuid.get(iuid);
        if (localFiles == null) {
            localFiles = new ArrayList<FileInfo>();
            localFilesByIuid.put(iuid, localFiles);
        }
        localFiles.add(fileInfo);
        notAvailableIuids.remove(iuid);
    }

    public Iterator getCUIDs() {
        return iuidsAndTsuidsByCuid.keySet().iterator();
    }

    public Set removeInstancesOfClass(String cuid) {
        IuidsAndTsuids iuidsAndTsuids = iuidsAndTsuidsByCuid.get(cuid);
        Iterator it = iuidsAndTsuids.iuids.iterator();
        String iuid;
        while (it.hasNext()) {
            iuid = (String) it.next();
            localFilesByIuid.remove(iuid);
        }
        return iuidsAndTsuids.iuids;
    }

    public final Collection getLocalFiles() {
        return localFilesByIuid.values();
    }
}
